
import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {
  public url:string;
  public Token:any;
  public urlAlbum:string;
  public Albums:any;

  constructor( private http:HttpClient) {
    this.url ='https://localhost:5001/ApiSpotify';
    this.urlAlbum = 'https://localhost:5001/GetAlbum?x='
   }

   getToken(){
     
      this.Token = this.http.get(this.url);
      return this.Token;

   }

   getAlbum(name:string){
     this.Albums = this.http.get(this.urlAlbum + name);
     return this.Albums;

   }

}
