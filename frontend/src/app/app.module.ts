
import { HttpClientModule } from '@angular/common/http';
// Class BrowserModule-> Busca Modulos en el Proyecto.
import { BrowserModule } from '@angular/platform-browser';
// Modulo Raiz del Sistema
import { NgModule } from '@angular/core';
// Modulos de Ruta
import { AppRoutingModule } from './app-routing.module';
// Componentente Principal de Angular
import { AppComponent } from './app.component';
import { BuscadorComponent } from './buscador/buscador.component';

/// Decoradores 
@NgModule({
  declarations: [
    AppComponent,
    BuscadorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
