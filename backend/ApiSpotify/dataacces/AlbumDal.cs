﻿using System;
using MongoDB.Driver;
using ApiSpotify.models;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver.Builders;

namespace ApiSpotify.dataacces
{
    public class AlbumDal
    {
        private readonly IMongoDatabase _database;

        public AlbumDal()
        {
            _database = Conectar();
        }

        /// <summary>
        ///  Lista todos los Albumnes del la BDD
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Allbum> Todos()
        {

            var producto = _database.GetCollection<Allbum>("allbum").Find(new BsonDocument()).ToListAsync();

            return producto.Result;

        }
        /// <summary>
        /// Insert todos los albunes que no existe en la BDD
        /// </summary>
        /// <param name="lista"></param>
        /// <returns></returns>
        public bool Insert(IList<Allbum> lista)
        {
            foreach(var prod in lista)
            {
                var productos = _database.GetCollection<BsonDocument>("allbum");
                var filter = Builders<BsonDocument>.Filter.Eq("id_album", prod.Id_Album);

                var result = productos.Find(filter);

                 if (result.CountDocuments() == 0)
                {
                    productos.InsertOne(prod.ToBsonDocument());
                }
            }

            return true;

        }

        private IMongoDatabase Conectar()
        {
            //var cliente = new MongoClient("mongodb://localhost:27017");
            var cliente = new MongoClient("mongodb+srv://jbarriga:jbarriga@cluster0-dlzda.mongodb.net/test");
            var database = cliente.GetDatabase("spotify");

            return database;


        }
    }
}
