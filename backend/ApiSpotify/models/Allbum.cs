﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ApiSpotify.models
{
    public class Allbum
    {
        [BsonId]
        public ObjectId id { get; set; }

        [BsonElement ("album_group")]
        public string AllBum_Group { get; set; }

        [BsonElement("album_type")]
        public string AlbumType { get; set; }

        [BsonElement("artists")]
        public List<Artista> Artistas { get; set; }

        [BsonElement("available_markets")]
        public List<string> AvailableMarkets { get; set; }

        [BsonElement("external_urls")]
        public Dictionary<string, string> ExternalUrls { get; set; }

        [BsonElement("href")]
        public string Href { get; set; }

        [BsonElement("id_album")]
        public string Id_Album { get; set; }

        [BsonElement("images")]
        public List<Image> Images { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("release_date")]
        public string ReleaseDate { get; set; }

        [BsonElement("release_date_precision")]
        public string ReleaseDatePrecision { get; set; }

        [BsonElement("restrictions")]
        public Dictionary<string,string> Restrictions { get; set; }

        [BsonElement("total_tracks")]
        public long TotalTracks { get; set; }

        [BsonElement("type")]
        public string Type { get; set; }

        [BsonElement("uri")]
        public string Uri { get; set; }

        [BsonElement("error")]
        public string Error { get; set; }
    }
}
