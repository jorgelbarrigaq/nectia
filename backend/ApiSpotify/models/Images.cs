﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace ApiSpotify.models
{
    public class Image
    {
        [BsonElement("url")]
        public string Url { get; set; }

        [BsonElement("width")]
        public long Width { get; set; }

        [BsonElement("height")]
        public long Height { get; set; }
    }

}
