﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace ApiSpotify.models
{
    public class Artista
    {
        [BsonElement("external_urls")]
        public Dictionary<string, string> ExternalUrls { get; set; }

        [BsonElement("href")]
        public string Href { get; set; }

        [BsonElement("id_artista")]
        public string Id_Artista { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("type")]
        public string Type { get; set; }

        [BsonElement("uri")]
        public string Uri { get; set; }

        [BsonElement("error")]
        public string Error { get; set; }
    }
}
