﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using System.Net.Http.Headers;
using SpotifyAPI.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Caching.Memory;
using ApiSpotify.models;
using ApiSpotify.dataacces;

namespace ApiSpotify.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ApiSpotifyController : ControllerBase
    {
        SpotifyAuthentication sAuth = new SpotifyAuthentication();

        private readonly IMemoryCache _cache;


        public ApiSpotifyController(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }

        [HttpGet]
        public ContentResult Get()
        {
            var qb = new QueryBuilder();
            qb.Add("response_type", "code");
            qb.Add("client_id", sAuth.clientID);
            qb.Add("scope", "user-read-private user-read-email");
            qb.Add("redirect_uri", sAuth.redirectURL);

            return new ContentResult
            {
                ContentType = "text/html",
                Content = @"
                    <!DOCTYPE html>
                    <html>
                        <head>
                            <meta charset=""utf-8"">
                            <title>Spotify Auth Example</title>
                        </head>
                        <body>
                            <script>
                             window.location = ""https://accounts.spotify.com/authorize/" + qb.ToQueryString().ToString() + @""";
                            </script>

                        </body>
                    </html>
                "
            };
        }

        [Route("/callback")]
        public ContentResult Get(string code)
        {
            string responseString = "";

            if (code.Length > 0)
            {
                using (HttpClient client = new HttpClient())
                {
                    Console.WriteLine(Environment.NewLine + "Your basic bearer: " + Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(sAuth.clientID + ":" + sAuth.clientSecret)));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(sAuth.clientID + ":" + sAuth.clientSecret)));

                    FormUrlEncodedContent formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("code", code),
                        new KeyValuePair<string, string>("redirect_uri", sAuth.redirectURL),
                        new KeyValuePair<string, string>("grant_type", "authorization_code"),
                    });

                    var response = client.PostAsync("https://accounts.spotify.com/api/token", formContent).Result;

                    var responseContent = response.Content;
                    responseString = responseContent.ReadAsStringAsync().Result;
                }
            }

            var jo = JObject.Parse(responseString);
           

            _cache.Set("token", jo["access_token"].ToString());

            return new ContentResult
            {
                ContentType = "application/json",
                Content = responseString
            };

        }

        [Route("/GetAlbum")]
        public ContentResult GetAlbum(string x)
        {
            SpotifyWebAPI api = new SpotifyWebAPI
            {
                AccessToken = _cache.Get("token").ToString(),
                TokenType = "Bearer"
            };


            SpotifyAPI.Web.Models.SearchItem album = api.SearchItems(x, SpotifyAPI.Web.Enums.SearchType.Album);

            var lista= GuardaResult(album);

            return new ContentResult
            {
                ContentType = "application/json",
                Content = JsonConvert.SerializeObject(lista)
            };

        }
        /// <summary>
        /// Guarda el Resultado en MongoDB Si no Existe
        /// </summary>
        /// <param name="obj"></param>
        public List<Allbum> GuardaResult(SpotifyAPI.Web.Models.SearchItem obj)
        {
            List<Allbum> _listaAlbum = new List<Allbum>(); 
            foreach (var album in obj.Albums.Items)
            {
                var NewAlbun = new Allbum();

                NewAlbun.AllBum_Group = album.AlbumGroup;
                NewAlbun.AlbumType = album.AlbumType;
                NewAlbun.Href = album.Href;
                NewAlbun.Id_Album = album.Id;
                NewAlbun.Name = album.Name;
                NewAlbun.ReleaseDate = album.ReleaseDate;
                NewAlbun.ReleaseDatePrecision = album.ReleaseDatePrecision;
                NewAlbun.Restrictions = album.Restrictions;
                NewAlbun.TotalTracks = album.TotalTracks;
                NewAlbun.Type = album.Type;
                NewAlbun.Uri = album.Uri;
                NewAlbun.Error = string.Empty;

                List<Artista> listart = new List<Artista>(); 
                foreach ( var artista in album.Artists)
                {
                    var newartista = new Artista();
                    newartista.Error = string.Empty;
                    newartista.ExternalUrls = artista.ExternalUrls;
                    newartista.Id_Artista = artista.Id;
                    newartista.Name = artista.Name;
                    newartista.Href = artista.Href;
                    newartista.Uri = artista.Uri;
                    newartista.Type = artista.Type;

                    listart.Add(newartista);
                }

                NewAlbun.Artistas = listart;
                NewAlbun.AvailableMarkets = album.AvailableMarkets;


                List<models.Image> listImage = new List<models.Image>();
                foreach (var _image in album.Images)
                {
                    var newImage = new models.Image();

                    newImage.Url = _image.Url;
                    newImage.Height = _image.Height;
                    newImage.Width = _image.Width;
                    listImage.Add(newImage);
                }

                NewAlbun.Images = listImage;

               _listaAlbum.Add(NewAlbun);
            }

            AlbumDal _dal = new AlbumDal();
            _dal.Insert(_listaAlbum);

            return _listaAlbum;

        }

        [Route("/api/GetAlbumes")]
        public IEnumerable<Allbum> GetAlbumes()
        {

            AlbumDal _dal = new AlbumDal();

            return _dal.Todos();
        }

    }
}

class SpotifyAuthentication
{
    public string clientID = "c92658aef33043c0acb0a0e66486aead";
    public string clientSecret = "0505bea0834c45a2bb54167384f93878";
    public string redirectURL = "https://localhost:5001/callback";
}