# Test Técnico #

El desafío es, mediante el uso de las credenciales generar un access_token que permite obtener una búsqueda de álbumes. 
Guardar dichos álbumes en una base de datos (sqlite o algún mongodb público) y exponer dichos resultados en una interfaz web.


### Estructura de Carpetas ###


![Scheme](./img/4.png)

## Capas
* Front-End Angular 9
* Api Rest .NET Core 2.2

### Back-End / Proyecto ApiSpotify .Net Core 2.2  ###

![Scheme](./img/2.png)

### Front-End / Angular 9 - TypeScript ###

![Scheme](./img/3.png)

## Guia de Usuarios ##

### Servicio API REST retorna Álbumes por Nombre

![Scheme](./img/5.png)

### Pagína de Inicio, Front con Angular.

* Ingresar el Álbum a Buscar
* Se desplega en Pantalla los Álbumes encontrados y se Guarda en la BDD Cloud de MongoDB

![Scheme](./img/1.png)

## Otros ##

### Credenciales Spotify.

 >
 **User ID: ** c92658aef33043c0acb0a0e66486aead
 >
 **Secret ID: ** 0505bea0834c45a2bb54167384f93878
 >
 ** Url Retorno:** https://localhost:5001/callback
 
 
### Credenciales MongoDB Cloud. 
 
  > 
  **string conexion:** mongodb+srv://jbarriga:jbarriga@cluster0-dlzda.mongodb.net/test
  > 
  **Base de Datos:** spotify
  > 
  **Collection:** allbum
  
#### Expoloración Collection con MongoDB Compass
 
 ![Scheme](./img/6.png)
 
  
